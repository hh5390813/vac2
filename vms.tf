##### VMs Part #####

resource "yandex_compute_instance" "debian" {
  name        = "debian"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
#      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd89dg1rq7uqslc6eigm" # debian-11
      size = 15
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[debian]
${yandex_compute_instance.debian.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning #####

resource "null_resource" "debian" {
  depends_on = [yandex_compute_instance.debian, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.debian.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u root -i inventory --key-file ~/.ssh/id_rsa debian.yml"
  }
}

